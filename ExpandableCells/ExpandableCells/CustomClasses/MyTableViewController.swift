//
//  MyTableViewController.swift
//  ExpandableCells
//
//  Created by Eugene Migun on 13/02/2017.
//  Copyright © 2017 eugenoid. All rights reserved.
//

import UIKit

class MyTableViewController: MyExpandableTableViewController
{
    override func viewDidLoad()
	{
        super.viewDidLoad()

		self.dataSource = self.testDataSource()
    }

	func testDataSource() -> [Dictionary<String, Any>]
	{
		return [
			["name": "walking",
			 "skills":
				[
					"beginner",
					"intermediate",
					"skilled"
				]
			],
			
			["name": "beginrunning"],
			
			["name": "runningforweightloss",
			 "skills":
				[
					["skill": "beginner",
					 "levels":
						[
							"level1",
							"level2"
						]
					],
					["skill": "intermediate",
					 "levels":
						[
							"level1",
							"level2"
						]
					],
					["skill": "skilled",
					 "levels":
						[
							"level1",
							"level2"
						]
					]
				]
			],
			
			["name": "trainer5"],

			["name": "trainer10"]
		]
	}
}
