//
//  MyExpandableTableViewController.swift
//  ExpandableCells
//
//  Created by Eugene Migun on 13/02/2017.
//  Copyright © 2017 eugenoid. All rights reserved.
//

import UIKit

class MyExpandableTableViewController: UITableViewController
{
	var expandedIndexPaths = [IndexPath]()
	
	var dataSource = [Dictionary<String, Any>]()
	
	
	
    override func viewDidLoad()
	{
        super.viewDidLoad()
		self.tableView.separatorStyle = .none
		
		self.tableView.register(UINib(nibName: "MyExpandableCell", bundle: nil), forCellReuseIdentifier: "MyExpandableCell")
    }

    override func numberOfSections(in tableView: UITableView) -> Int
	{
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return self.dataSource.count > 0 ? self.dataSource.count : 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: "MyExpandableCell", for: indexPath) as! MyExpandableCell
		cell.cellInfo = self.dataSource[indexPath.row]
		cell.isBottom = (indexPath.row == self.dataSource.count-1)
        return cell
	}
	
	override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
	{
		if let cell = cell as? MyExpandableCell
		{
			let expanded = expandedIndexPaths.contains(indexPath)
			cell.setExpanded(expanded, animated: false)
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if let cell = tableView.cellForRow(at: indexPath) as? MyExpandableCell
		{
			if(MyExpandableUtils.canExpandCell(self.dataSource[indexPath.row]))
			{
				self.toggleCell(cell, animated: true)
			}
		}
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		if(!self.expandedIndexPaths.contains(indexPath))
		{
			if(indexPath.row == self.dataSource.count-1)
			{
				return 44
			}
			return 44 + 15
		}
		return MyExpandableUtils.calculateExpandedCellHeight(self.dataSource[indexPath.row], isBottom: (indexPath.row == self.dataSource.count-1))
	}
	
	func toggleCell(_ cell: MyExpandableCell, animated: Bool)
	{
		if !cell.expanded
		{
			self.expandCell(cell, animated: animated)
		}
		else
		{
			self.collapseCell(cell, animated: animated)
		}
	}
	
	func expandCell(_ cell: MyExpandableCell, animated: Bool)
	{
		if let indexPath = tableView.indexPath(for: cell)
		{
			if !animated
			{
				cell.setExpanded(true, animated: false)
				addToExpandedIndexPaths(indexPath)
			}
			else
			{
				CATransaction.begin()
				
				CATransaction.setCompletionBlock(
				{ () -> Void in
					// 2. animate views after expanding
					cell.setExpanded(true, animated: true)
				})
				
				// 1. expand cell height
				tableView.beginUpdates()
				self.addToExpandedIndexPaths(indexPath)
				tableView.endUpdates()
				
				CATransaction.commit()
			}
		}
	}
	
	func collapseCell(_ cell: MyExpandableCell, animated: Bool)
	{
		if let indexPath = tableView.indexPath(for: cell)
		{
			if !animated
			{
				cell.setExpanded(false, animated: false)
				self.removeFromExpandedIndexPaths(indexPath)
			}
			else
			{
				CATransaction.begin()
				
				CATransaction.setCompletionBlock(
				{ () -> Void in
					// 2. collapse cell height
					self.tableView.beginUpdates()
					self.removeFromExpandedIndexPaths(indexPath)
					self.tableView.endUpdates()
				})
				
				// 1. animate views before collapsing
				cell.setExpanded(false, animated: true)
				
				CATransaction.commit()
			}
		}
	}
	
	fileprivate func addToExpandedIndexPaths(_ indexPath: IndexPath)
	{
		self.expandedIndexPaths.append(indexPath)
	}
	
	fileprivate func removeFromExpandedIndexPaths(_ indexPath: IndexPath)
	{
		if let index = self.expandedIndexPaths.index(of: indexPath)
		{
			self.expandedIndexPaths.remove(at: index)
		}
	}
	
	func detailViewForIndexPath(_ indexPath: IndexPath) -> UIView?
	{
		return nil
	}
}
