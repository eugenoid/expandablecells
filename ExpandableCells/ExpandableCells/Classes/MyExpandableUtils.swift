//
//  MyExpandableUtils.swift
//  ExpandableCells
//
//  Created by Eugene Migun on 14/02/2017.
//  Copyright © 2017 eugenoid. All rights reserved.
//

import UIKit

class MyExpandableUtils: NSObject
{
	static func canExpandCell(_ cellInfo: Dictionary<String, Any>) -> Bool
	{
		if(cellInfo["skills"] == nil)
		{
			return false
		}
		return true
	}
	
	static func calculateExpandedCellHeight(_ cellInfo: Dictionary<String, Any>, isBottom: Bool) -> CGFloat
	{
		var cellHeight: CGFloat = 44 //header
		
		cellHeight += self.calculateDetailViewHeight(cellInfo) //detail view
		
		if(!isBottom)
		{
			cellHeight += 15 //bottom arrow
		}

		//print("calculateExpandedCellHeight: \(cellHeight)")
		
		return cellHeight
	}

	static func calculateDetailViewHeight(_ cellInfo: Dictionary<String, Any>) -> CGFloat
	{
		if(cellInfo["skills"] == nil)
		{
			//print("calculateDetailViewHeight: 0")

			return 0
		}
		
		let skills = cellInfo["skills"] as! Array<Any>
		//print("skills: \(skills)")
		
		var detailHeight: CGFloat = 0
		
		for i in 0 ..< skills.count
		{
			let skill = skills[i]
			//print("skill: \(skill)")
			
			if(skill is String)
			{
				//print("skill is string")
				
				detailHeight += 28 //detail row height
			}
			else if (skill is Dictionary<String, Any>)
			{
				//print("skill is dictionary: \(skill)")
				
				let skillName: String = (skill as! [String:Any])["skill"] as! String
				//print("skill name: \(skillName)")
				
				detailHeight += 28 //section header
				
				
				let skillLevels = (skill as! [String:Any])["levels"] as! [String]
				
				//print("skillLevels: \(skillLevels)")
				
				for _ in 0 ..< skillLevels.count
				{
					detailHeight += 28
				}
			}
			else
			{
				print("skill is unknown type: \(String(describing: type(of: skill)))")
			}
		}
		
		//print("calculateDetailViewHeight: \(detailHeight)")

		return detailHeight
	}

}
