//
//  MyExpandableCell.swift
//  ExpandableCells
//
//  Created by Eugene Migun on 13/02/2017.
//  Copyright © 2017 eugenoid. All rights reserved.
//

import UIKit

class MyExpandableCell: UITableViewCell
{
	var expanded: Bool = false
	
	var cellInfo = Dictionary<String, Any>()
	
	var isBottom: Bool = false
	
	@IBOutlet weak var headerView: UIView!
	@IBOutlet weak var verticalLineView: UIView!
	@IBOutlet weak var arrowView: UIView!
	@IBOutlet weak var detailView: UITableView!
	
	func setExpanded(_ expanded: Bool, animated: Bool)
	{
		self.expanded = expanded
		
		if !animated
		{
			self.updateCell()
		}
		else
		{
			let alwaysOptions: UIViewAnimationOptions = [.allowUserInteraction, .beginFromCurrentState, .transitionCrossDissolve]
			let expandedOptions: UIViewAnimationOptions = [.curveEaseOut]
			let collapsedOptions: UIViewAnimationOptions = [.curveEaseIn]
			let options: UIViewAnimationOptions = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
			
			UIView.transition(with: self.detailView, duration: 0.3, options: options, animations:
			{
				() -> Void in
				self.updateCell()
			}, completion: nil)
		}
	}
	
	func updateCell()
	{
		self.detailView.isHidden = !expanded
	}
	
	override func layoutSubviews()
	{
		super.layoutSubviews()
		
		self.headerView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.headerView.bounds.height)
		self.detailView.frame = CGRect(x: 20, y: self.headerView.bounds.height, width: self.bounds.width, height: MyExpandableUtils.calculateDetailViewHeight(self.cellInfo))
		
		if(self.isBottom)
		{
			self.arrowView.frame = CGRect.zero
			self.arrowView.isHidden = true
			self.verticalLineView.frame = CGRect.zero
			self.verticalLineView.isHidden = true
		}
		else
		{
			self.verticalLineView.frame = CGRect(x: 10, y: self.headerView.bounds.height, width: 4, height: self.bounds.height - self.headerView.bounds.height - 15)
			self.verticalLineView.isHidden = false
			self.arrowView.frame = CGRect(x: 10, y: self.bounds.height - 15, width: 4, height: 15)
			self.arrowView.isHidden = false
		}
		
		/*
		print("cell bounds: \(self.bounds)")
		print("-------------------")
		print("header frame: \(self.headerView.frame)")
		print("detailView frame: \(self.detailView.frame)")
		print("verticalLineView frame: \(self.verticalLineView.frame)")
		print("arrowView frame: \(self.arrowView.frame)")
		*/
	}
	
}


extension MyExpandableCell: UITableViewDataSource, UITableViewDelegate
{
	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
		if(cell == nil)
		{
			cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
		}
		
		//cell?.textLabel?.text = "aaa"
		
		
		let skills = cellInfo["skills"] as! Array<Any>
		
		var skill = skills[0]
		if(skill is String)
		{
			let skillName = skills[indexPath.row] as? String
			cell?.textLabel?.text = skillName
		}
		else
		{
			skill = skills[indexPath.section]
			let skillLevels = (skill as! [String:Any])["levels"] as! [String]

			let skillName = skillLevels[indexPath.row]
			
			cell?.textLabel?.text = skillName
		}
		
		
		//print("skillLevels: \(skillLevels)")

		
		
		
		return cell!
	}
	
	public func numberOfSections(in tableView: UITableView) -> Int
	{
		if(cellInfo["skills"] == nil)
		{
			return 0
		}
		
		let skills = cellInfo["skills"] as! Array<Any>
		
		//берем первый попавшийся скилл. Если это просто строка, то будет отдна секция с названиями скиллов
		let skill = skills[0]
		if(skill is String)
		{
			return 1
		}
		
		//у нас вложенные скиллы
		return skills.count
	}

	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		let skills = cellInfo["skills"] as! Array<Any>
		
		let skill = skills[section]
		if(skill is String)
		{
			return skills.count
		}
		else if (skill is Dictionary<String, Any>)
		{
			let skillLevels = (skill as! [String:Any])["levels"] as! [String]
			//print("skillLevels: \(skillLevels)")
			return skillLevels.count
		}
		else
		{
			print("skill is unknown type: \(String(describing: type(of: skill)))")
		}

		return 0
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
	{
		let skills = cellInfo["skills"] as! Array<Any>
		
		//берем первый попавшийся скилл. Если это просто строка, то будет отдна секция с названиями скиллов
		let skill = skills[0]
		if(skill is String)
		{
			return 0
		}

		return 28
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
	{
		let skills = cellInfo["skills"] as! Array<Any>
		let skill = skills[section]
		
		if(skill is String)
		{
			return nil
		}
		
		let skillName: String = (skill as! [String:Any])["skill"] as! String
		return skillName
	}
}



